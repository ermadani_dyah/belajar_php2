<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di Sanbercode!"
*/

// Code function di sini


// Hapus komentar untuk menjalankan code!
// greetings("Bagas");
// greetings("Wahyu");
// greetings("Abdul");

function greetings($nama) {
    echo "Halo $nama, Selamat Datang di Sanbercode!";
}
greetings("Bagas");
echo "<br>";
function greetings2($nama) {
    echo "Halo $nama, Selamat Datang di Sanbercode!";
}
greetings2("Wahyu");
echo "<br>";
function greetings3($nama) {
    echo "Halo $nama, Selamat Datang di Sanbercode!";
}
greetings3("Abdul");

echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>";
/* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

// Code function di sini 


// Hapus komentar di bawah ini untuk jalankan Code
// reverseString("abduh");
// reverseString("Sanbercode");
// reverseString("We Are Sanbers Developers")
function Reverse($string){
    $reverse = '';
    $i = 0;
    while(!empty($string[$i])){ 
        $reverse = $string[$i].$reverse; 
        $i++;
    }
    return $reverse;
}
  
// Driver Code
$str = "abduh";
echo Reverse($str);
echo "<br>";

function Reverse2($string){
    $reverse = '';
    $i = 0;
    while(!empty($string[$i])){ 
        $reverse = $string[$i].$reverse; 
        $i++;
    }
    return $reverse;
}
  
// Driver Code
$str = "Sanbercode";
echo Reverse2($str);
echo "<br>";

function Reverse3($string){
    $reverse = '';
    $i = 0;
    while(!empty($string[$i])){ 
        $reverse = $string[$i].$reverse; 
        $i++;
    }
    return $reverse;
}
  
// Driver Code
$str = "We Are Sanbers Developers";
echo Reverse3($str);
echo "<br>";

echo "<h3>Soal No 3 Palindrome </h3>";
/* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

*/


// Code function di sini

// Hapus komentar di bawah ini untuk jalankan code
// palindrome("civic") ; // true
// palindrome("nababan") ; // true
// palindrome("jambaban"); // false
// palindrome("racecar"); // true
function Reverse4($string){
    $reverse = '';
    $i = 0;
    while(!empty($string[$i])){ 
        $reverse = $string[$i].$reverse; 
        $i++;
    }
    $kata= $reverse;
    if($string==$kata){
        $kalimat="$string Ini Bernilai benar";
    }else{
        $kalimat="$string Ini bernilai salah";
    }
    return $kalimat;
}
  
// Driver Code
$str = "civic";
echo Reverse4($str);
echo "<br>";

function Reverse5($string){
    $reverse = '';
    $i = 0;
    while(!empty($string[$i])){ 
        $reverse = $string[$i].$reverse; 
        $i++;
    }
    $kata= $reverse;
    if($string==$kata){
        $kalimat="$string Ini Bernilai benar";
    }else{
        $kalimat="$string Ini bernilai salah";
    }
    return $kalimat;
}
  
// Driver Code
$str = "nababan";
echo Reverse5($str);
echo "<br>";

function Reverse6($string){
    $reverse = '';
    $i = 0;
    while(!empty($string[$i])){ 
        $reverse = $string[$i].$reverse; 
        $i++;
    }
    $kata= $reverse;
    if($string==$kata){
        $kalimat="$string Ini Bernilai benar";
    }else{
        $kalimat="$string Ini bernilai salah";
    }
    return $kalimat;
}
  
// Driver Code
$str = "jambaban";
echo Reverse6($str);
echo "<br>";


function Reverse7($string){
    $reverse = '';
    $i = 0;
    while(!empty($string[$i])){ 
        $reverse = $string[$i].$reverse; 
        $i++;
    }
    $kata= $reverse;
    if($string==$reverse){
        $kalimat="$string Ini Bernilai benar";
    }else{
        $kalimat="$string Ini bernilai salah";
    }
    return $kalimat;
}
  
// Driver Code
$str = "racecar";
echo Reverse7($str);
echo "<br>";

echo "<h3>Soal No 4 Tentukan Nilai";
/*
Soal 4
buatlah sebuah file dengan nama tentukan-nilai.php. Di dalam file tersebut buatlah function dengan nama tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini

// Hapus komentar di bawah ini untuk jalankan code
// echo tentukan_nilai(98); //Sangat Baik
// echo tentukan_nilai(76); //Baik
// echo tentukan_nilai(67); //Cukup
// echo tentukan_nilai(43); //Kurang

function tentukan_nilai($nilai){
    if(($nilai<100)&&($nilai>=85)){
        return"Sangat Baik";
    }else if(($nilai<85)&&($nilai>=70)){
        return "Baik";
    }else if (($nilai<70)&&($nilai>=60)){
        return "Cukup";
    }else{
        return"Sangat Kurang";
    }
}
echo "<br>";
echo tentukan_nilai(98);
echo "<br>";
echo tentukan_nilai(76);
echo "<br>";
echo tentukan_nilai(67);
echo "<br>";
echo tentukan_nilai(43);
?>
</body>

</html>